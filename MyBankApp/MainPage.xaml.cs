﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MyBankApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        SavingsAccount account1 = new SavingsAccount("111-001", "Bob", 2000, 2.0);
        SavingsAccount account2 = new SavingsAccount("111-002", "Mary", 5000, 2.0);

        public MainPage()
        {
            this.InitializeComponent();

        }

        // View details
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            account1Txt.Text = account1.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            account2Txt.Text = account2.ToString();
        }

        // Deposit
        private void account1DepositBtn_Click(object sender, RoutedEventArgs e)
        {
            account1.Deposit(1000);
            account1Txt.Text = account1.ToString();
        }

        private void account2DepositBtn_Click(object sender, RoutedEventArgs e)
        {
            account2.Deposit(1000);
            account2Txt.Text = account2.ToString();
        }


        // Withdraw
        private void account1WithdrawBtn_Click(object sender, RoutedEventArgs e)
        {
            bool withdraw = account1.Withdraw(1000);

            if (withdraw)
            {
                account1Txt.Text = account1.ToString();
            }
             else
            {
                account1Txt.Text = "Not enough money";
            }
        }

        private void account2WithdrawBtn_Click(object sender, RoutedEventArgs e)
        {
            bool withdraw = account2.Withdraw(1000);

            if (withdraw)
            {
                account2Txt.Text = account2.ToString();
            }
            else
            {
                account2Txt.Text = "Not enough money";
            }
        }


        // Display Interest
        private void account1InterestBtn_Click(object sender, RoutedEventArgs e)
        {
            account1Txt.Text = string.Format("Interest Rate is: " + account1.CalculateInterest());
        }

        private void account2InterestBtn_Click(object sender, RoutedEventArgs e)
        {
            account2Txt.Text = string.Format("Interest Rate is: " + account2.CalculateInterest());
        }
    }
}
