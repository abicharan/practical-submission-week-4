﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp
{
    class SavingsAccount : BankAccount
    {
        private double rate;

        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }

        public SavingsAccount (): base() { }

        public SavingsAccount (string accNo, string accName, double balance, double rate) : base(accNo, accName, balance)
        {
            this.rate = rate;
        }


        public double CalculateInterest ()
        {
            return Balance*((rate/100)); //PlaceHolder
        }

        public override string ToString ()
        {
            return base.ToString() + " Rate: " + rate;
        }

    }
}
