﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBankApp
{
    class BankAccount
    {   
        // Creating the variables
        private string accNo;
        private string accName;
        private double balance;


        // What do u call these again?
        public string AccNo
        {
            get { return accNo; }
            set { accNo = value; }
        }


        public string AccName
        {
            get { return accName; }
            set { accName = value; }
        }


        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        //Constructors
        public BankAccount() { }

        public BankAccount (string accNo, string accName, double balance)
        {
            this.accNo = accNo;
            this.accName = accName;
            this.balance = balance;
        }


        // Methods

        public void Deposit (double value)
        {
            balance = balance + value;
        }

        public bool Withdraw (double valueTaken)
        {
            if (valueTaken <= balance)
            {
                balance = balance - valueTaken;
                return true;
            } else
            {
                return false;
            }
        }

        public double EnquireBalance ()
        {
            return balance;
        }

        public override string ToString ()
        {
            return string.Format("Account Name : {0}, Account Number: {1}, Account Balance {2}", accName, accNo, balance);
        }

    }
}
